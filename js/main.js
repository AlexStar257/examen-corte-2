let tg1 = 0;
let tg2 = 0;
var opt_1 = "";
var opt_2 = "";
var opt_3 = "";
var opt_4 = "";
var origen = "";
var destino = "";
/*  1.- Mexicano
    2.- Estados Unidos
    3.- Canada
    4.- Euro

    5.- Mexicano
    6.- Estados Unidos
    7.- Canada
    8.- Euro
*/
function calcular() {
    let cantidad = document.getElementById('monto');
    let origen = parseInt(document.getElementById('origen').value);
    let destino = parseInt(document.getElementById('destino').value);
    let subtotal = document.getElementById('subtotal')
    let comision = document.getElementById('comision')
    let total = document.getElementById('total')

    destino = document.opciones.destino[document.opciones.destino.selectedIndex].value;

    // Mexicano a Estado Unidense
    if (origen == "1" && destino == "Dólar Estadounidense") {

        subtotal.value = cantidad.value * 19.85;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

        // Mexicano a Canada
    } else if (origen == "1" && destino == "Dólar Canadiense") {

        subtotal.value = cantidad.value * 19.85;
        subtotal.value = subtotal.value * 1.35;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

    }// Mexicano a Euro
    else if (origen == "1" && destino == "Euros") {
        subtotal.value = cantidad.value * 19.85;
        subtotal.value = subtotal.value * .99;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);
    }
    else if (origen == "2" && destino == "Pesos Mexicanos") {

        subtotal.value = cantidad.value * 19.85;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);
    }
    else if (origen == "2" && destino == "Dólar Estadounidense") {
        subtotal.value = cantidad.value * 1.35;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);
    } else if (origen == "2" && destino == "Dólar Canadiense") {

        subtotal.value = cantidad.value * .99;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

        // Canada a Mexicano
    } else if (origen == "3" && destino == "Pesos Mexicanos") {

        subtotal.value = cantidad.value * 19.85;
        subtotal.value = subtotal.value * 1.35;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

        // Canada a Estados Unidos
    } else if (origen == "3" && destino == "Dólar Estadounidense") {

        subtotal.value = cantidad.value * 1.35;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

    }// Canada a Euro
    else if (origen == "3" && destino == "Euros") {

        subtotal.value = cantidad.value * 1.35;
        subtotal.value = cantidad.value * .99;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

    }
    else if (origen == "4" && destino == "Pesos Mexicanos") {

        subtotal.value = cantidad.value * 19.85;
        subtotal.value = subtotal.value * .99;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

    } else if (origen == "4" && destino == "Dólar Estadounidense") {
        subtotal.value = cantidad.value * .99;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);
    } else if (origen == "4" && destino == "Dólar Canadiense") {

        subtotal.value = cantidad.value * 1.35;
        subtotal.value = subtotal.value * .99;
        comision.value = subtotal.value * 0.03;
        total.value = parseInt(subtotal.value) + parseInt(comision.value);

    }


}



function registrar() {
    let cantidad = document.getElementById('monto');
    let origen = parseInt(document.getElementById('origen').value);
    let destino = parseInt(document.getElementById('destino').value);
    let subtotal = document.getElementById('subtotal')
    let comision = document.getElementById('comision')
    let total = document.getElementById('total')

    let textos = document.getElementById('textos');
    // Mexicano a Estado Unidense
    if (origen == "1" && destino == "6") {

        textos.innerHTML = cantidad.value + " Pesos Mexicano a Dolar Estado Unidense = " + total.value;

        // Mexicano a Canada
    } else if (origen == "1" && destino == "7") {

        textos.innerHTML = cantidad.value + " Pesos Mexicano a Dolar Canadiense = " + total.value;

    }// Mexicano a Euro
    else if (origen == "1" && destino == "8") {
        textos.innerHTML = cantidad.value + " Pesos Mexicano a Euro = " + total.value;
    }
    else if (origen == "2" && destino == "5") {
        textos.innerHTML = cantidad.value + " Dolar Estado Unidense a Pesos Mexicano = " + total.value;

    }
    else if (origen == "2" && destino == "7") {
        textos.innerHTML = cantidad.value + " Dolar Estado Unidense a Dolar Canadiense = " + total.value;
    } else if (origen == "2" && destino == "8") {

        textos.innerHTML = cantidad.value + " Dolar Estado Unidense a Euro = " + total.value;

        // Canada a Mexicano
    } else if (origen == "3" && destino == "5") {

        textos.innerHTML = cantidad.value + " Dolar Canadiense a Pesos Mexicano = " + total.value;

        // Canada a Estados Unidos
    } else if (origen == "3" && destino == "6") {

        textos.innerHTML = cantidad.value + " Dolar Canadiense a Dolar Estado Unidense = " + total.value;

    }// Canada a Euro
    else if (origen == "3" && destino == "8") {

        textos.innerHTML = cantidad.value + " Dolar Canadiense a Euro = " + total.value;

    }
    else if (origen == "4" && destino == "5") {
        textos.innerHTML = cantidad.value + " Euro a Pesos Mexicano = " + total.value;


    } else if (origen == "4" && destino == "6") {
        textos.innerHTML = cantidad.value + " Euro a Pesos Dolar Estado Unidense = " + total.value;
    } else if (origen == "4" && destino == "7") {
        textos.innerHTML = cantidad.value + " Euro a Pesos Dolar Canadiense = " + total.value;

    }


    let TotalG = document.getElementById('totalG');

    
    tg1 = parseInt(total.value);
    tg2 = tg1 + tg2;
    TotalG.innerHTML = tg2;


}

function borrar(){

    document.getElementById('monto').value = "";
    document.getElementById('origen').value = "-1";
    document.getElementById('destino').value = "-2";
    document.getElementById('subtotal').value = "";
    document.getElementById('comision').value = "";
    document.getElementById('total').value = "";
    document.getElementById('textos').innerHTML = "";
    document.getElementById('totalG').innerHTML = "";
    

}



    function Change() {
        
        opt_1 = new Array("Moneda Destino", "Dólar Estadounidense", "Dólar Canadiense", "Euros");
        opt_2 = new Array("Moneda Destino", "Pesos Mexicanos", "Dólar Canadiense", "Euros");
        opt_3 = new Array("Moneda Destino", "Dólar Estadounidense", "Pesos Mexicanos", "Euros");
        opt_4 = new Array("Moneda Destino", "Dólar Estadounidense", "Pesos Mexicanos", "Dólar Canadiense");
    
        var op;
        op = document.opciones.origen[document.opciones.origen.selectedIndex].value;
    
        if (op != 0) {
            Opt = eval("opt_" + op);
            numOpt = Opt.length;
            document.opciones.destino.length = numOpt;
            for (i = 0; i < numOpt; i++) {
                document.opciones.destino.options[i].value = Opt[i];
                document.opciones.destino.options[i].text = Opt[i];
            }
        } else {
            document.opciones.destino.length = 1;
            document.opciones.destino.options[0].value = "-";
            document.opciones.destino.options[0].text = "-";
        }
        document.opciones.destino.options[0].selected = true;
    }
    
    





